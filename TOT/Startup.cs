﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TOT.Startup))]
namespace TOT
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
