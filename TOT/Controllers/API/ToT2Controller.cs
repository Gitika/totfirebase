﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TOT.Controllers.API
{
    public class ToT2Controller : ApiController
    {
        [System.Web.Http.HttpGet]
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/ToT2/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/ToT2
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/ToT2/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ToT2/5
        public void Delete(int id)
        {
        }
    }
}
