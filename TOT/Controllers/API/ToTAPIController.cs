﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

using TOT.BAL;
using TOT.Models;

namespace TOT.Controllers.API
{
    public class ToTAPIController : ApiController
    {
        [AllowAnonymous]
        [HttpPost]
        public IHttpActionResult SendMailToSupplier([FromBody]SupplierModel objSupplierModel)
        {
            ResponseModel objResponseModel = new ResponseModel();

            try
            {
                string signUpTemplate = System.Web.HttpContext.Current.Server.MapPath("~/EmailTemplates/supplier-invitation.html");

                string strBody = Helper.ReadEmailTemplate(signUpTemplate);

                strBody = strBody.Replace("[UserName]", objSupplierModel.Name);
                strBody = strBody.Replace("[CompanyName]", objSupplierModel.CompanyName);
                strBody = strBody.Replace("[ActivationCode]", objSupplierModel.ActivationCode);
                strBody = strBody.Replace("[CoName]", ConfigurationManager.AppSettings["CoName"].ToString());

                Helper.SendMail(objSupplierModel.ToEmailID, ConfigurationManager.AppSettings["SupportMail"].ToString(), ConfigurationManager.AppSettings["SupplierMailSubject"].ToString(), strBody);

                objResponseModel.Status = HttpStatusCode.OK;
                objResponseModel.Message = "Success";
                
                return Ok(objResponseModel);
            }
            catch (Exception ex)
            {
                objResponseModel.Status = HttpStatusCode.InternalServerError;
                objResponseModel.Message = ex.Message;

                Helper.SendExceptionMail("ToTAPIController", "SendMailToSupplier", ex.Message + "----" + ex.StackTrace);

                return InternalServerError(ex);
            }
        }
    }

    public class ResponseModel
    {
        public HttpStatusCode Status { get; set; }
        public string Message { get; set; }
    }
}
