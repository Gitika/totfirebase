﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

using TOT.BAL;
using TOT.Models;

namespace TOT.Controllers
{
    public class BuyerController : Controller
    {
        [AllowAnonymous]
        public ActionResult ActivateAccount(string buyerID)
        {
            BuyerModel model = new BuyerModel();
            CompanyModel objCompanyModel = new CompanyModel();

            string sBID = Helper.Decrypt(HttpUtility.UrlDecode(buyerID));

            model = CompanyRepository.GetBuyerDetail(sBID);

            if (model != null)
            {
                if (model.CompanyID == "0")
                    ViewBag.CompanyName = model.CompanyName;
                else
                {
                    objCompanyModel = CompanyRepository.GetCompanyDetails(model.CompanyID);
                    ViewBag.CompanyName = (objCompanyModel != null) ? objCompanyModel.CompanyName : "";
                }

                ViewBag.ProfileCompleted = (model.Password == "" || model.Password == null) ? "0" : "1";
            }
            else
                ViewBag.ProfileCompleted = "1";

            ViewBag.buyerID = sBID;
            ViewBag.CompanyName = (model != null) ? ((model.CompanyName == "" || model.CompanyName == null) ? "" : model.CompanyName) : "";
            ViewBag.AcVerified = "0";
            ViewBag.EmailID = "";
            ViewBag.Pwd = "";

            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ActivateAccount(BuyerModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Password))
            {
                ModelState.AddModelError("PasswordReq", "Please enter password");
            }
            else if (model.Password.Length < 6)
            {
                ModelState.AddModelError("PasswordReq", "Password should be atleast 6 characters");
            }
            else
            {
                var result = CompanyRepository.ActivateBuyer(model);
                //model.Password = "";
                //return RedirectToAction("ProfileCompleted", "Buyer");

                BuyerModel objBuyerModel = CompanyRepository.GetBuyerDetail(model.UserID);

                ViewBag.ProfileCompleted = "-1";
                ViewBag.AcVerified = "1";
                ViewBag.EmailID = (objBuyerModel != null) ? objBuyerModel.EmailID : "";
                ViewBag.Pwd = model.Password;

                return View(model);
            }

            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult RecreateBuyerByKey(string buyerID, string userUID)
        {
            BuyerModel model = new BuyerModel();
            BuyerModel objBuyerModel = new BuyerModel();
            CompanyModel objCompanyModel = new CompanyModel();

            model = CompanyRepository.GetBuyerDetail(buyerID);

            objBuyerModel.UserID = userUID;
            objBuyerModel.City = model.City;
            objBuyerModel.CompanyID = model.CompanyID;
            objBuyerModel.CompanyName = model.CompanyName;
            objBuyerModel.ContactName = model.ContactName;
            objBuyerModel.DeviceType = model.DeviceType;
            objBuyerModel.EmailID = model.EmailID;
            objBuyerModel.Password = model.Password;
            objBuyerModel.FirstName = model.FirstName;
            objBuyerModel.IsActive = model.IsActive;
            objBuyerModel.IsAuthenticated = model.IsAuthenticated;
            objBuyerModel.IsDeleted = model.IsDeleted;
            objBuyerModel.IsNotificationsRequired = model.IsNotificationsRequired;
            objBuyerModel.JobTitle = model.JobTitle;
            objBuyerModel.LastName = model.LastName;
            objBuyerModel.Phone = model.Phone;
            objBuyerModel.UserType = model.UserType;
            objBuyerModel.DateCreated = model.DateCreated;
            objBuyerModel.DateUpdated = model.DateUpdated;

            bool bInsertResult = CompanyRepository.InsertBuyerWithKey(objBuyerModel, userUID);

            bool bDeleteResult = CompanyRepository.DeleteBuyerPermanently(buyerID);

            if (bInsertResult && bDeleteResult)
                return Json(new { status = "1", message = "" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { status = "0", message = "Something went wrong. Please contact administrator." }, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult ProfileCompleted()
        {
            return View();
        }
    }
}