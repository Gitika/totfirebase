﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

using TOT.BAL;
using TOT.Models;
using TOT.Utility;

namespace TOT.Controllers
{
    [TotErrorHandler]
    [CompanyAuthorization]
    public class CompanyController : Controller
    {
        public Models.CompanyModel CurrentCompany
        {
            get
            {
                if (Session["CompanyID"] != null)
                {
                    return Session["CompanyID"] as Models.CompanyModel;
                }
                else
                {
                    return null;
                }
            }
        }

        public JsonResult GetCompany()
        {
            CompanyModel model = new CompanyModel();

            model = CompanyRepository.GetCompanyDetails(CurrentCompany.UserID);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult ActivateAccount(string companyID)
        {
            CompanyModel model = new CompanyModel();

            string sCID = Helper.Decrypt(HttpUtility.UrlDecode(companyID));

            model = CompanyRepository.GetCompanyDetails(sCID);

            if (model != null)
                ViewBag.ProfileCompleted = (model.Password == "" || model.Password == null) ? "0" : "1";
            else
                ViewBag.ProfileCompleted = "1";

            ViewBag.companyID = sCID;
            ViewBag.AcVerified = "0";
            ViewBag.EmailID = "";
            ViewBag.Pwd = "";

            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ActivateAccount(CompanyModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Password))
            {
                ModelState.AddModelError("PasswordReq", "Please enter password");
            }
            else if (model.Password.Length < 6)
            {
                ModelState.AddModelError("PasswordReq", "Password should be atleast 6 characters");
            }
            else
            {
                var result = CompanyRepository.ActivateAccount(model);
                model.FirstName = model.FirstName + " " + model.LastName;
                //return RedirectToAction("Login", "Company");

                CompanyModel objCompanyModel = CompanyRepository.GetCompanyDetails(model.UserID);

                ViewBag.ProfileCompleted = "-1";
                ViewBag.AcVerified = "1";
                ViewBag.EmailID = (objCompanyModel != null) ? objCompanyModel.EmailID : "";
                ViewBag.Pwd = model.Password;
            }
            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult RecreateCompanyByKey(string companyID, string userUID)
        {
            CompanyModel model = new CompanyModel();
            CompanyModel objCompanyModel = new CompanyModel();
            //CompanyModel objCompanyModel = new CompanyModel();

            model = CompanyRepository.GetCompanyDetails(companyID);

            objCompanyModel.UserID = userUID;
            objCompanyModel.City = model.City;
            objCompanyModel.CompanyID = model.CompanyID;
            objCompanyModel.CompanyName = model.CompanyName;
            objCompanyModel.ContactName = model.ContactName;
            objCompanyModel.CountryID = model.CountryID;
            objCompanyModel.DeviceType = model.DeviceType;
            objCompanyModel.EmailID = model.EmailID;
            objCompanyModel.Password = model.Password;
            objCompanyModel.FirstName = model.FirstName;
            objCompanyModel.IsActive = model.IsActive;
            objCompanyModel.IsAuthenticated = model.IsAuthenticated;
            objCompanyModel.IsDeleted = model.IsDeleted;
            objCompanyModel.IsNotificationsRequired = model.IsNotificationsRequired;
            objCompanyModel.JobTitle = model.JobTitle;
            objCompanyModel.LastName = model.LastName;
            objCompanyModel.Phone = model.Phone;
            objCompanyModel.UserType = model.UserType;
            objCompanyModel.DateCreated = model.DateCreated;
            objCompanyModel.DateUpdated = model.DateUpdated;

            bool bInsertResult = CompanyRepository.InsertCompanyWithKey(objCompanyModel, userUID);

            bool bDeleteResult = CompanyRepository.DeleteCompanyPermanently(companyID);

            if (bInsertResult && bDeleteResult)
                return Json(new { status = "1", message = "" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { status = "0", message = "Something went wrong. Please contact administrator." }, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        //[ValidateAntiForgeryToken()]
        public ActionResult Login(Models.CompanyModel model)
        {
            Models.CompanyModel objCompanyUserModel = null;

            if (ModelState.IsValid)
            {
                objCompanyUserModel = BAL.CompanyRepository.Authenticate(model.EmailID, model.Password);

                if (objCompanyUserModel != null)
                {
                    if (objCompanyUserModel.IsAuthenticated)
                    {
                        Session["CompanyID"] = objCompanyUserModel;
                        return RedirectToAction("Buyers", "Company");
                    }
                    else
                    {
                        ModelState.AddModelError("InvalidCredential", objCompanyUserModel.ErrorMessage);
                    }
                }
            }

            return View(objCompanyUserModel);
        }

        public ActionResult EditProfile()
        {
            CompanyModel model = new CompanyModel();

            ViewBag.CountryList = CompanyRepository.GetCountryList();

            model = CompanyRepository.GetCompanyDetails(CurrentCompany.UserID);

            return View(model);
        }

        [HttpPost]
        public ActionResult SaveCompany(CompanyModel model)
        {
            bool bResult = false;

            if (CurrentCompany != null)
            {
                bResult = CompanyRepository.UpdateCompany(model);

                if (bResult)
                    return RedirectToAction("EditProfile", "Company");
                else
                {
                    ViewBag.CountryList = CompanyRepository.GetCountryList();

                    return View(model);
                }
            }
            else
            {
                return RedirectToAction("Login", "Company");
            }
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult ForgotPassword(string EmailID)
        {
            CompanyModel objCompanyModel = null;

            objCompanyModel = CompanyRepository.GetCompanyDetailsByEmailID(EmailID);

            if (objCompanyModel != null)
            {
                string strCID = HttpUtility.UrlEncode(Helper.Encrypt(objCompanyModel.UserID.ToString()));

                string strTinyURL = Helper.MakeTinyUrl(ConfigurationManager.AppSettings["SiteURL"].ToString() + Url.Action("ResetPassword", "Company", new { companyID = strCID }));

                //Send Confirmation Mail
                string strBody = "<font face='arial' size='2'><br>Hello, <br><br>";
                strBody = strBody + "We've received a request to reset your password. If you didn't make the request, just ignore this email. Otherwise, you can reset your password using this link:";
                strBody = strBody + "<br /><br /><br /><p style='text-align:center;margin:0;padding:0;'><b style='border:2px solid #1655a8;background-color:#1655a8;padding:12px 80px 14px 80px;'>";
                strBody = strBody + "<a href='" + strTinyURL + "' ";
                strBody = strBody + "style='font-weight:bold;text-decoration:none;font-size:16px;color:#ffffff' target='_blank'>Reset Password</a></b></p>";
                strBody = strBody + "<br /><br />Thanks,";
                strBody = strBody + "<br />The " + ConfigurationManager.AppSettings["CoName"].ToString() + " Team</font>";

                Helper.SendMail(objCompanyModel.EmailID, ConfigurationManager.AppSettings["SupportMail"].ToString(), "Reset Your Password :: " + ConfigurationManager.AppSettings["CoName"].ToString(), strBody);
            }
            else
            {
                ModelState.AddModelError("Email", "User does not exist");
            }

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string companyID)
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult ResetPassword(string companyID, string password)
        {
            string sCID = Helper.Decrypt(HttpUtility.UrlDecode(companyID));

            var result = CompanyRepository.ResetPassword(sCID, password);

            if (result)
                return Json("Success", JsonRequestBehavior.AllowGet);
            else
                return Json("Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Buyers()
        {
            return View();
        }

        public ActionResult GetBuyersList(string searchText, int currentPage)
        {
            if (CurrentCompany != null)
            {
                int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
                int totalCount = 0;
                var lstBuyers = CompanyRepository.GetBuyersList(CurrentCompany.UserID, searchText, currentPage, pageSize, out totalCount);
                ViewBag.TotalSearchCount = totalCount;
                ViewBag.PageSize = pageSize;
                ViewBag.PageIndex = currentPage;

                return PartialView("~/Views/Company/Shared/_BuyerList.cshtml", lstBuyers);
            }
            else
            {
                return RedirectToAction("Login", "Company");
            }
        }

        public ActionResult AddBuyer()
        {
            BuyerModel model = new BuyerModel();
            model.IsActive = true;
            return View(model);
        }

        public ActionResult EditBuyer(string userId)
        {
            BuyerModel model = new BuyerModel();

            model = CompanyRepository.GetBuyerDetail(userId);

            return View(model);
        }

        [HttpPost]
        public ActionResult SaveBuyer(BuyerModel model, bool IsDeactivated = false)
        {
            bool bResult = false;
            string buyerID = "";

            if (CurrentCompany != null)
            {
                //Add Mode
                if (model.UserID == null)
                {
                    CompanyModel objCompanyModel = (CompanyModel)Session["CompanyID"];

                    if (!CompanyRepository.IsBuyerExist("", objCompanyModel.UserID, model.EmailID))
                    {
                        model.CompanyID = objCompanyModel.UserID;
                        model.CompanyName = objCompanyModel.CompanyName;
                        model.IsActive = !IsDeactivated;
                        model.IsDeleted = false;
                        model.UserType = Helper.UserType.Buyer.ToString();
                        model.DeviceType = "Web";
                        model.DateCreated = DateTime.Now.ToLocalTime().ToString();
                        model.DateUpdated = DateTime.Now.ToLocalTime().ToString();
                        //model.UserID = "pqrs";

                        bResult = CompanyRepository.InsertBuyer(model, out buyerID);

                        if (bResult)
                        {
                            string strBID = HttpUtility.UrlEncode(Helper.Encrypt(buyerID));

                            string strTinyURL = Helper.MakeTinyUrl(ConfigurationManager.AppSettings["SiteURL"].ToString() + Url.Action("ActivateAccount", "Buyer", new { buyerID = strBID }));

                            string FilePath = Server.MapPath("~/EmailTemplates/buyer-invitation.html");

                            StreamReader sr = new StreamReader(FilePath);
                            string strContent = sr.ReadToEnd();

                            StringBuilder stbHTML = new StringBuilder();

                            stbHTML.Append(strContent);

                            stbHTML = stbHTML.Replace("[BuyerName]", model.FirstName + " " + model.LastName);
                            stbHTML = stbHTML.Replace("[ActivationLink]", strTinyURL);
                            stbHTML = stbHTML.Replace("[CoName]", ConfigurationManager.AppSettings["CoName"].ToString());

                            Helper.SendMail(model.EmailID, ConfigurationManager.AppSettings["SupportMail"].ToString(), objCompanyModel.CompanyName + " has invited you to join :: " + ConfigurationManager.AppSettings["CoName"].ToString(), stbHTML.ToString());
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("SaveBuyerError", "Buyer already exist");
                        return View("~/Views/Company/AddBuyer.cshtml", model);
                    }
                }
                else //Edit Mode
                {
                    CompanyModel objCompanyModel = (CompanyModel)Session["CompanyID"];

                    if (!CompanyRepository.IsBuyerExist(model.UserID, objCompanyModel.UserID, model.EmailID))
                    {
                        model.IsActive = !IsDeactivated;

                        bResult = CompanyRepository.UpdateBuyer(model);
                    }
                    else
                    {
                        ModelState.AddModelError("SaveBuyerError", "Buyer already exist");
                        return View("~/Views/Company/EditBuyer.cshtml", model);
                    }
                }

                if (bResult)
                    return RedirectToAction("Buyers", "Company");
                else
                    return View(model);
            }
            else
            {
                return RedirectToAction("Login", "Company");
            }
        }

        [HttpPost]
        public JsonResult DeleteBuyer(string userId)
        {
            bool result = CompanyRepository.DeleteBuyer(userId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Seasons()
        {
            return View();
        }

        public ActionResult GetSeasonsList(string searchText, int currentPage)
        {
            if (CurrentCompany != null)
            {
                int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
                int totalCount = 0;
                var lstBuyers = CompanyRepository.GetSeasonsList(CurrentCompany.UserID, searchText, currentPage, pageSize, out totalCount);
                ViewBag.TotalSearchCount = totalCount;
                ViewBag.PageSize = pageSize;
                ViewBag.PageIndex = currentPage;

                return PartialView("~/Views/Company/Shared/_SeasonList.cshtml", lstBuyers);
            }
            else
            {
                return RedirectToAction("Login", "Company");
            }
        }

        public JsonResult GetSeasonDetails(string seasonId)
        {
            SeasonsModel model = new SeasonsModel();

            model = CompanyRepository.GetSeasonDetail(seasonId);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SaveSeason(SeasonsModel objSeasonsModel)
        {
            bool bResult = false;

            if (CurrentCompany != null)
            {
                //Add Mode
                if (objSeasonsModel.ID == null)
                {
                    CompanyModel objCompanyModel = (CompanyModel)Session["CompanyID"];

                    if (!CompanyRepository.IsSeasonExist("", objCompanyModel.UserID, objSeasonsModel.Name))
                    {
                        objSeasonsModel.UserID = objCompanyModel.UserID;
                        objSeasonsModel.IsDeleted = false;
                        objSeasonsModel.DateCreated = DateTime.Now.ToLocalTime().ToString();
                        objSeasonsModel.DateUpdated = DateTime.Now.ToLocalTime().ToString();

                        bResult = CompanyRepository.InsertSeason(objSeasonsModel);
                    }
                    else
                    {
                        return Json(new { result = "Season already exist" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else //Edit Mode
                {
                    CompanyModel objCompanyModel = (CompanyModel)Session["CompanyID"];

                    if (!CompanyRepository.IsSeasonExist(objSeasonsModel.ID, objCompanyModel.UserID, objSeasonsModel.Name))
                    {
                        objSeasonsModel.UserID = objCompanyModel.UserID;
                        objSeasonsModel.DateUpdated = DateTime.Now.ToLocalTime().ToString();
                        
                        bResult = CompanyRepository.UpdateSeason(objSeasonsModel);
                    }
                    else
                    {
                        return Json(new { result = "Season already exist" }, JsonRequestBehavior.AllowGet);
                    }
                }

                if (bResult)
                    return Json(new { result = "Season saved successfully." }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { result = "Error while saving season." }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { result = "Session expired" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteSeason(string seasonId)
        {
            bool result = CompanyRepository.DeleteSeason(seasonId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Login", "Company");
        }
    }
}
