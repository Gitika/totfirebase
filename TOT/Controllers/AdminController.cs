﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.IO;
using System.Text;

using TOT.Models;
using TOT.BAL;
using TOT.Utility;

namespace TOT.Controllers
{
    [TotErrorHandler]
    [AdminAuthorization]
    public class AdminController : Controller
    {
        public AdminUsersModel CurrentAdminUser
        {
            get
            {
                if (Session["AdminUserID"] != null)
                {
                    return Session["AdminUserID"] as AdminUsersModel;
                }
                else
                {
                    return null;
                }
            }
        }

        public JsonResult GetAdminUser()
        {
            AdminUsersModel model = new AdminUsersModel();

            model = AdminUserRepository.GetAdminUserDetails(CurrentAdminUser.AdminUserID);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        //[ValidateAntiForgeryToken()]
        public ActionResult Login(AdminUsersModel model)
        {
            AdminUsersModel objAdminUsersModel = null;

            if (ModelState.IsValid)
            {
                objAdminUsersModel = AdminUserRepository.Authenticate(model.EmailID, model.Password);

                if (objAdminUsersModel != null)
                {
                    if (objAdminUsersModel.IsAuthenticated)
                    {
                        Session["AdminUserID"] = objAdminUsersModel;
                        return RedirectToAction("Companies", "Admin");
                    }
                    else
                    {
                        ModelState.AddModelError("InvalidCredential", objAdminUsersModel.ErrorMessage);
                    }
                }
            }

            return View(objAdminUsersModel);
        }

        public ActionResult EditProfile()
        {
            AdminUsersModel model = new AdminUsersModel();

            model = AdminUserRepository.GetAdminUserDetails(CurrentAdminUser.AdminUserID);

            return View(model);
        }

        [HttpPost]
        public ActionResult SaveAdmin(AdminUsersModel model)
        {
            bool bResult = false;

            if (CurrentAdminUser != null)
            {
                bResult = AdminUserRepository.UpdateAdminUser(model);

                if (bResult)
                    return RedirectToAction("EditProfile", "Admin");
                else
                    return View(model);
            }
            else
            {
                return RedirectToAction("Login", "Admin");
            }
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult ForgotPassword(string EmailID)
        {
            AdminUsersModel objAdminUsersModel = null;

            objAdminUsersModel = AdminUserRepository.GetAdminUserDetailsByEmailID(EmailID);

            if (objAdminUsersModel != null)
            {
                string strAID = HttpUtility.UrlEncode(Helper.Encrypt(objAdminUsersModel.AdminUserID.ToString()));

                string strTinyURL = Helper.MakeTinyUrl(ConfigurationManager.AppSettings["SiteURL"].ToString() + Url.Action("ResetPassword", "Admin", new { adminUserID = strAID }));

                //Send Confirmation Mail
                string strBody = "<font face='arial' size='2'><br>Hello, <br><br>";
                strBody = strBody + "We've received a request to reset your password. If you didn't make the request, just ignore this email. Otherwise, you can reset your password using this link:";
                strBody = strBody + "<br /><br /><br /><p style='text-align:center;margin:0;padding:0;'><b style='border:2px solid #1655a8;background-color:#1655a8;padding:12px 80px 14px 80px;'>";
                strBody = strBody + "<a href='" + strTinyURL + "' ";
                strBody = strBody + "style='font-weight:bold;text-decoration:none;font-size:16px;color:#ffffff' target='_blank'>Reset Password</a></b></p>";
                strBody = strBody + "<br /><br />Thanks,";
                strBody = strBody + "<br />The " + ConfigurationManager.AppSettings["CoName"].ToString() + " Team</font>";

                Helper.SendMail(objAdminUsersModel.EmailID, ConfigurationManager.AppSettings["SupportMail"].ToString(), "Reset Your Password :: " + ConfigurationManager.AppSettings["CoName"].ToString(), strBody);
            }
            else
            {
                ModelState.AddModelError("Email", "User does not exist");
            }

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string adminUserID)
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult ResetPassword(string adminUserID, string password)
        {
            string sAID = Helper.Decrypt(HttpUtility.UrlDecode(adminUserID));

            var result = AdminUserRepository.ResetPassword(sAID, password);

            if (result)
                return Json("Success", JsonRequestBehavior.AllowGet);
            else
                return Json("Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Companies()
        {
            return View();
        }

        public ActionResult Dashboard()
        {
            return View();
        }

        public ActionResult GetCompanyList(string searchText, int currentPage)
        {
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            int totalCount = 0;
            var lstCompany = CompanyRepository.GetCompanyList(searchText, currentPage, pageSize, out totalCount);
            ViewBag.TotalSearchCount = totalCount;
            ViewBag.PageSize = pageSize;
            ViewBag.PageIndex = currentPage;

            return PartialView("~/Views/Admin/Shared/_CompanyList.cshtml", lstCompany);
        }

        public ActionResult AddCompany()
        {
            CompanyModel model = new CompanyModel();
            ViewBag.CountryList = CompanyRepository.GetCountryList();
            ViewBag.PgMode = "Add";
            return View(model);
        }

        public ActionResult EditCompany(string userID)
        {
            CompanyModel model = new CompanyModel();

            model = CompanyRepository.GetCompanyDetails(userID);

            //var selectedItem = model.CountryList.First(p => p.Value == model.CountryID.ToString());

            //if (selectedItem != null)
            //{
            //    selectedItem.Selected = true;
            //}

            ViewBag.CountryList = CompanyRepository.GetCountryList();

            return View(model);
        }

        public JsonResult GetCountryList()
        {
            List<CountryModel> lstCountryModel = new List<CountryModel>();
            //lstCountryModel = CompanyRepository.GetCountryList();
            return Json(lstCountryModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveCompany(CompanyModel model)
        {
            bool bResult = false;
            string userID = "";

            if (CurrentAdminUser != null)
            {
                //Add Mode
                if (model.UserID == null)
                {
                    if (!CompanyRepository.IsCompanyExist("", model.EmailID))
                    {
                        model.CompanyID = "0";
                        model.IsActive = true;
                        model.IsDeleted = false;
                        model.UserType = Helper.UserType.Company.ToString();
                        model.DeviceType = "Web";
                        model.DateCreated = DateTime.Now.ToLocalTime().ToString();
                        model.DateUpdated = DateTime.Now.ToLocalTime().ToString();

                        bResult = CompanyRepository.InsertCompany(model, out userID);

                        if (bResult)
                        {
                            string strCID = HttpUtility.UrlEncode(Helper.Encrypt(userID));

                            string strTinyURL = Helper.MakeTinyUrl(ConfigurationManager.AppSettings["SiteURL"].ToString() + Url.Action("ActivateAccount", "Company", new { companyID = strCID }));

                            string FilePath = Server.MapPath("~/EmailTemplates/company-invitation.html");

                            StreamReader sr = new StreamReader(FilePath);
                            string strContent = sr.ReadToEnd();

                            StringBuilder stbHTML = new StringBuilder();

                            stbHTML.Append(strContent);

                            stbHTML = stbHTML.Replace("[UserName]", model.FirstName + " " + model.LastName);
                            stbHTML = stbHTML.Replace("[ActivationLink]", strTinyURL);
                            stbHTML = stbHTML.Replace("[CoName]", ConfigurationManager.AppSettings["CoName"].ToString());

                            Helper.SendMail(model.EmailID, ConfigurationManager.AppSettings["SupportMail"].ToString(), "You have been invited to join :: " + ConfigurationManager.AppSettings["CoName"].ToString(), stbHTML.ToString());
                        }
                    }
                    else
                    {
                        ViewBag.CountryList = CompanyRepository.GetCountryList();

                        ModelState.AddModelError("SaveCompanyError", "Company already exist");
                        return View("~/Views/Admin/AddCompany.cshtml", model);
                    }
                }
                else //Edit Mode
                {
                    if (!CompanyRepository.IsCompanyExist(model.UserID, model.EmailID))
                    {
                        bResult = CompanyRepository.UpdateCompany(model);
                    }
                    else
                    {
                        ViewBag.CountryList = CompanyRepository.GetCountryList();

                        ModelState.AddModelError("SaveCompanyError", "Company already exist");
                        return View("~/Views/Admin/EditCompany.cshtml", model);
                    }
                }

                if (bResult)
                    return RedirectToAction("Companies", "Admin");
                else
                {
                    ViewBag.CountryList = CompanyRepository.GetCountryList();

                    return View(model);
                }
            }
            else
            {
                return RedirectToAction("Login", "Admin");
            }
        }

        [HttpPost]
        public JsonResult DeleteCompany(string UserID)
        {
            bool result = CompanyRepository.DeleteCompany(UserID);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Login", "Admin");
        }
    }
}
