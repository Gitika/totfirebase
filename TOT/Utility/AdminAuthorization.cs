﻿using System.Web.Mvc;
using System.Web.Routing;
using System.Web;

using TOT.Models;

namespace TOT.Utility
{
    public class AdminAuthorization : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext.Session["AdminUserID"] != null)
            {
                AdminUsersModel model = (httpContext.Session["AdminUserID"] as AdminUsersModel);
                return model.IsAuthenticated;
            }
            return false;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.Result = new RedirectToRouteResult(new
                RouteValueDictionary(new { controller = "Admin", action = "Login" }));
            }
            else
            {
                HttpContext.Current.Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
                HttpContext.Current.Response.End();
            }
        }
    }
}