﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web;

using TOT.Models;

namespace TOT.Utility
{
    public class CompanyAuthorization : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext.Session["CompanyID"] != null)
            {
                CompanyModel model = (httpContext.Session["CompanyID"] as CompanyModel);
                return model.IsAuthenticated;
            }
            return false;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.Request.IsAjaxRequest())
            {
                filterContext.Result = new RedirectToRouteResult(new
                RouteValueDictionary(new { controller = "Company", action = "Login" }));
            }
            else
            {
                HttpContext.Current.Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
                HttpContext.Current.Response.End();
            }
        }
    }
}