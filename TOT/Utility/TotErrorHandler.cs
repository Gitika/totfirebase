﻿using System;
using System.Web.Mvc;

using TOT.BAL;

namespace TOT.Utility
{
    public class TotErrorHandler : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            string controllerName = Convert.ToString(filterContext.RouteData.Values["controller"]);
            string actionName = Convert.ToString(filterContext.RouteData.Values["action"]);
            string action = string.Empty;
            string controller = string.Empty;

            if (!string.IsNullOrEmpty(actionName))
                action = actionName;

            if (!string.IsNullOrEmpty(controllerName))
                controller = controllerName;

            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            Helper.SendExceptionMail(controller, action, ex.Message, "***", "***");
            var model = new HandleErrorInfo(filterContext.Exception, "Controller", "Action");

            filterContext.Result = new ViewResult()
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(model.Exception)
            };
        }
    }
}