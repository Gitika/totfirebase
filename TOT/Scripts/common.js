function ShowAlert(msg) {
    alert(msg);
}

function URLEncodeCP(clearString) {
    var output = '';
    var x = 0;
    clearString = clearString.toString();
    var regex = /(^[a-zA-Z0-9_.]*)/;
    while (x < clearString.length) {
        var match = regex.exec(clearString.substr(x));
        if (match != null && match.length > 1 && match[1] != '') {
            output += match[1];
            x += match[1].length;
        } else {
            if (clearString[x] == ' ')
                output += '+';
            else {
                var charCode = clearString.charCodeAt(x);
                var hexVal = charCode.toString(16);
                output += '%' + (hexVal.length < 2 ? '0' : '') + hexVal.toUpperCase();
            }
            x++;
        }
    }
    return output;
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function isNumberNDotKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        if (charCode == 46)
            return true;
        else
            return false;
    }

    return true;
}

function ValidatePhone(evt) {
    //allow Number, dash & space
    var charCode = (evt.which) ? evt.which : event.keyCode
    console.log("charcode is " + charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        if (charCode == 32 || charCode == 43 || charCode == 40 || charCode == 41)
            return true;
        else
            return false;
    }

    return true;
}

function ValidateEmail(email) {
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return expr.test(email);
};

var specialKeys = new Array();
specialKeys.push(8); //Backspace
specialKeys.push(9); //Tab
specialKeys.push(46); //Delete
specialKeys.push(36); //Home
specialKeys.push(35); //End
specialKeys.push(37); //Left
specialKeys.push(39); //Right

function ValidatePostcode(evt) {
    //allow Numbers, aplhabets & space
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (((charCode >= 48 && charCode <= 57) || (charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || charCode == 32 || (specialKeys.indexOf(evt.charCode) != -1 && evt.charCode != evt.charCode))) {
        return true;
    }
    else
        return false;

    return true;
}

/*This function is called in the Page_Load event of MasterGeneralPage.Master.cs
This function is used when user tries to open the any CP page by rigth click "open
in new window" or "open in new tab"*/
function transferToIndexPageCP(strIndexUrl) {
    window.location.href = strIndexUrl + "?url=" + URLEncodeCP(window.location.href + '');
}




function RemoveComma(amount) {

    while (1) {
        if (amount.indexOf(',') > -1) {
            amount = amount.replace(',', '');

        }
        else
            return amount;
    }
    return amount;
}

function CurrencyFormatted(amount) {
    var i = parseFloat(amount);
    if (isNaN(i)) { i = 0.00; }
    var minus = '';
    if (i < 0) { minus = '-'; }
    i = Math.abs(i);
    i = parseInt((i + .005) * 100);
    i = i / 100;
    s = new String(i);
    if (s.indexOf('.') < 0) { s += '.00'; }
    if (s.indexOf('.') == (s.length - 2)) { s += '0'; }
    s = minus + s;
    return s;
}

function CommaFormatted(amount) {
    var delimiter = ",";
    var a = amount.split('.', 2)
    var d = a[1];
    var i = parseInt(a[0]);
    if (isNaN(i)) { return ''; }
    var minus = '';
    if (i < 0) { minus = '-'; }
    i = Math.abs(i);
    var n = new String(i);
    var a = [];
    while (n.length > 3) {
        var nn = n.substr(n.length - 3);
        a.unshift(nn);
        n = n.substr(0, n.length - 3);
    }
    if (n.length > 0) { a.unshift(n); }
    n = a.join(delimiter);
    if (d.length < 1) { amount = n; }
    else { amount = n + '.' + d; }
    amount = minus + amount;
    return '$' + amount;
}

function cellMouseOver(theCell, theClass) {
    eval(theCell).className = theClass;
}

function cellMouseOut(theCell, theClass) {
    eval(theCell).className = theClass;
}
function ShowAlert(msg) {
    alert(msg);
}

function reLoadPage(shouldRefresh) { if (shouldRefresh) { refresh(); } }

function SwitchMenu(obj) {
    if (document.getElementById) {
        var el = document.getElementById(obj);
        el.style.height = '';
        var ar = document.getElementById("masterdiv").getElementsByTagName("span");
        if (el.style.display != "block") {
            for (var i = 0; i < ar.length; i++) {
                if (ar[i].className == "SubMenu")
                    ar[i].style.display = "none";
            }
            el.style.display = "block";
        } else {

        }
    }
}


function get_cookie(Name) {
    var search = Name + "="
    var returnvalue = "";
    if (document.cookie.length > 0) {
        offset = document.cookie.indexOf(search)
        if (offset != -1) {
            offset += search.length
            end = document.cookie.indexOf(";", offset);
            if (end == -1) end = document.cookie.length;
            returnvalue = unescape(document.cookie.substring(offset, end))
        }
    }
    return returnvalue;
}

function onloadfunction() {
    if (persistmenu == "yes") {
        var cookiename = (persisttype == "sitewide") ? "switchmenu" : window.location.pathname
        var cookievalue = get_cookie(cookiename)
        if (cookievalue != "")
            document.getElementById(cookievalue).style.display = "block"
    }
}

function savemenustate() {
    var inc = 1, blockid = ""
    while (document.getElementById("sub" + inc)) {
        if (document.getElementById("sub" + inc).style.display == "block") {
            blockid = "sub" + inc
            break
        }
        inc++
    }
    var cookiename = (persisttype == "sitewide") ? "switchmenu" : window.location.pathname
    var cookievalue = (persisttype == "sitewide") ? blockid + ";path=/" : blockid
    document.cookie = cookiename + "=" + cookievalue
}

function showConstraint() {
    if (csc.value == 1) {
        SwitchMenu('sub1');
    }
    else if (csc.value == 2) {
        SwitchMenu('sub2');
    }
    else if (csc.value == 3) {
        SwitchMenu('sub3');
    }
    else if (csc.value == 4) {
        SwitchMenu('sub4');
    }
    else if (csc.value == 5) {
        SwitchMenu('sub5');
    }
    else {
        sub1.style.display = "none";
        sub2.style.display = "none";
        sub3.style.display = "none";
        sub4.style.display = "none";
        sub5.style.display = "none";
    }
}

function toggleBox(szDivID, iState) {
    if (document.layers) {
        document.layers[szDivID].visibility = iState ? "show" : "hide";

    }
    else if (document.getElementById) {
        var obj = document.getElementById(szDivID);
        obj.style.visibility = iState ? "visible" : "hidden";
    }
    else if (document.all) {
        document.all[szDivID].style.visibility = iState ? "visible" : "hidden";
    }
}

function correctPNG() /*correctly handle PNG transparency in Win IE 5.5 & 6.*/ {
    var arVersion = navigator.appVersion.split("MSIE")
    var version = parseFloat(arVersion[1])
    if ((version >= 5.5) && (document.body.filters)) {
    }
}

function OnClientClose() {
    document.body.onclick = "";
}
function MainWinClosed(radWindow) {
    window.location.reload(true);
}