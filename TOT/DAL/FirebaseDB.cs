﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;

namespace TOT.DAL
{
    public class FirebaseDB : IDisposable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FirebaseDB"/> class with base url of Firebase Database
        /// </summary>
        /// <param name="baseURL">Firebase Database URL</param>
        public FirebaseDB(string baseURL)
        {
            this.RootNode = baseURL;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FirebaseDB"/> class with base url of Firebase Database
        /// </summary>
        /// <param name="baseURL">Firebase Database URL</param>
        /// <param name="pathToJSONKey">Path To your JSON Key</param>
        public FirebaseDB(string baseURL, string pathToJSONKey, params string[] scopes)
        {
            this.RootNode = baseURL;
            AuthHelper.GenenateAccessToken(pathToJSONKey, scopes);
        }

        /// <summary>
        /// Gets or sets Represents current full path of a Firebase Database resource
        /// </summary>
        private string RootNode { get; set; }

        /// <summary>
        /// Adds more node to base URL
        /// </summary>
        /// <param name="node">Single node of Firebase DB</param>
        /// <returns>Instance of FirebaseDB</returns>
        public FirebaseDB Node(string node)
        {
            if (node.Contains("/"))
            {
                throw new FormatException("Node must not contain '/', use NodePath instead.");
            }

            return new FirebaseDB(this.RootNode + '/' + node);
        }

        /// <summary>
        /// Adds more nodes to base URL
        /// </summary>
        /// <param name="nodePath">Nodepath of Firebase DB</param>
        /// <returns>Instance of FirebaseDB</returns>
        public FirebaseDB NodePath(string nodePath)
        {
            return new FirebaseDB(this.RootNode + '/' + nodePath);
        }

        /// <summary>
        /// Make Get request
        /// </summary>
        /// <returns>Firebase Response</returns>
        public FirebaseResponse Get()
        {
            return new FirebaseRequest(HttpMethod.Get, this.RootNode).Execute();
        }

        /// <summary>
        /// Make Put request
        /// </summary>
        /// <param name="jsonData">JSON string to PUT</param>
        /// <returns>Firebase Response</returns>
        public FirebaseResponse Put(string jsonData)
        {
            return new FirebaseRequest(HttpMethod.Put, this.RootNode, jsonData).Execute();
        }

        /// <summary>
        /// Make Post request
        /// </summary>
        /// <param name="jsonData">JSON string to POST</param>
        /// <returns>Firebase Response</returns>
        public FirebaseResponse Post(string jsonData)
        {
            return new FirebaseRequest(HttpMethod.Post, this.RootNode, jsonData).Execute();
        }

        /// <summary>
        /// Make Patch request
        /// </summary>
        /// <param name="jsonData">JSON sting to PATCH</param>
        /// <returns>Firebase Response</returns>
        public FirebaseResponse Patch(string jsonData)
        {
            return new FirebaseRequest(new HttpMethod("PATCH"), this.RootNode, jsonData).Execute();
        }

        /// <summary>
        /// Make Delete request
        /// </summary>
        /// <returns>Firebase Response</returns>
        public FirebaseResponse Delete()
        {
            return new FirebaseRequest(HttpMethod.Delete, this.RootNode).Execute();
        }

        /// <summary>
        /// To String
        /// </summary>
        /// <returns>Current resource URL as string</returns>
        public override string ToString()
        {
            return this.RootNode;
        }

        public HttpWebResponse ExecuteWebRequest(string method, string jsonData)
        {
            HttpWebResponse objHttpResponse = null;

            WebRequest objWebRequest = null;
            
            try
            {
                objWebRequest = WebRequest.CreateHttp(this.RootNode);
                objWebRequest.Method = method;

                objWebRequest.ContentType = "application/json";
                var objBuffer = Encoding.UTF8.GetBytes(jsonData);
                objWebRequest.ContentLength = objBuffer.Length;

                //objWebRequest.Headers.Add("Authorization", "Bearer AIzaSyC8AL-i5kqHi65Va8CZqoZYZ0sF2gxYMYc");

                objWebRequest.GetRequestStream().Write(objBuffer, 0, objBuffer.Length);

                objHttpResponse = (HttpWebResponse)objWebRequest.GetResponse();

                //if (objHttpResponse.StatusCode == HttpStatusCode.OK)
                //{
                    //string objResponse = (new StreamReader(objHttpResponse.GetResponseStream())).ReadToEnd();

                    //Dictionary<string, object> objFBResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(objResponse);

                    //newFirebaseKey = (objFBResponse != null) ? objFBResponse["name"].ToString() : "";

                    //var objJavaScriptSerializer = new JavaScriptSerializer();

                    //var objWebResp = objJavaScriptSerializer.Deserialize<IDictionary<string, object>>(objResponse);

                    //if (objWebResp != null)
                    //{
                    //    var json1 = JsonConvert.SerializeObject(new
                    //    {
                    //        iAdminUserID = objWebResp["name"].ToString()
                    //    });

                    //    objWebRequest1 = WebRequest.CreateHttp(this.RootNode.Replace(".json", "") + "/" + objWebResp["name"].ToString() + "/.json");
                    //    objWebRequest1.Method = "PATCH";
                    //    objWebRequest1.ContentType = "application/json";
                    //    var objBuffer1 = Encoding.UTF8.GetBytes(json1);
                    //    objWebRequest1.ContentLength = objBuffer1.Length;
                    //    objWebRequest1.GetRequestStream().Write(objBuffer1, 0, objBuffer1.Length);

                    //    HttpWebResponse objHttpResponse1 = (HttpWebResponse)objWebRequest1.GetResponse();

                    //    if (objHttpResponse1.StatusCode == HttpStatusCode.OK)
                    //        strAdminUserID = objWebResp["name"].ToString();
                    //}
                //}
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("InsertDetails.cs", "AdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objWebRequest = null;
            }

            return objHttpResponse;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~FirebaseDB() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}