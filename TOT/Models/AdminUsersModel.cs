﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace TOT.Models
{
    public class AdminUsersModel
    {
        public string AdminUserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string City { get; set; }

        [Required(ErrorMessage = "Please enter email")]
        [EmailAddress(ErrorMessage = "Please enter valid email")]
        public string EmailID { get; set; }

        [Required(ErrorMessage = "Please enter password")]
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public bool IsAuthenticated { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class AdminUserList
    {
        public List<AdminUsersModel> AdminUsers { get; set; }
    }
}