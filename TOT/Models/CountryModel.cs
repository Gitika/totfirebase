﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOT.Models
{
    public class CountryModel
    {
        public string CountryID { get; set; }
        public string CountryName { get; set; }
        public string IsActive { get; set; }
        public string IsDeleted { get; set; }
    }
}