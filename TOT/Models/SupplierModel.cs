﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOT.Models
{
    public class SupplierModel
    {
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public string ActivationCode { get; set; }
        public string ToEmailID { get; set; }
    }
}