﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TOT.Models
{
    public class CompanyModel
    {
        public string UserID { get; set; }
        public string CompanyID { get; set; }
        public string UserType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string EmailID { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string ProfilePic { get; set; }
        public string JobTitle { get; set; }
        public string Phone { get; set; }
        public string City { get; set; }
        public IEnumerable<SelectListItem> CountryList { get; set; }
        public string CountryID { get; set; }
        public string DeviceID { get; set; }
        public string DeviceType { get; set; }
        public string AppVersion { get; set; }
        public string LastLoginOn { get; set; }
        public bool IsNotificationsRequired { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string DateCreated { get; set; }
        public string DateUpdated { get; set; }

        public string ContactName
        {
            get { return FirstName + " " + LastName; }
            set { }
        }

        public string ErrorMessage { get; set; }
        public bool IsAuthenticated { get; set; }
    }
}