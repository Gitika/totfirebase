﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOT.Models
{
    public class SeasonsModel
    {
        public string UserID { get; set; }
        public string ID { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string DateCreated { get; set; }
        public string DateUpdated { get; set; }
    }
}