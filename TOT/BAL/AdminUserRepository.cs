﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;

using TOT.Models;
using TOT.DAL;

namespace TOT.BAL
{
    public class AdminUserRepository
    {
        public static AdminUsersModel Authenticate(string EmailID, string Password)
        {
            FirebaseDB objFirebaseDB = new FirebaseDB(Helper.FirebaseURL + "AdminUsers.json");
            AdminUsersModel objAdminUsersModel = new AdminUsersModel();

            try
            {
                string objFBResponse = objFirebaseDB.Get().JSONContent;

                var objJavaScriptSerializer = new JavaScriptSerializer();

                //objAdminUsersModel = Newtonsoft.Json.JsonConvert.DeserializeObject<AdminUsersModel>(objFBResponse);
                //AdminUserList objAdminUserList = Newtonsoft.Json.JsonConvert.DeserializeObject<AdminUserList>(objFBResponse);

                var objAdminUserList = objJavaScriptSerializer.Deserialize<IDictionary<string, AdminUsersModel>>(objFBResponse);

                if (objAdminUserList != null && objAdminUserList.Count > 0)
                {
                    foreach (var AUser in objAdminUserList.Keys)
                    {
                        AdminUsersModel objAdmin = (AdminUsersModel)objAdminUserList[AUser];

                        if (EmailID == objAdmin.EmailID)
                        {
                            if (Password == objAdmin.Password)
                            {
                                objAdminUsersModel = objAdmin;
                                objAdminUsersModel.IsAuthenticated = true;
                            }
                            else
                            {
                                objAdminUsersModel.IsAuthenticated = false;
                                objAdminUsersModel.ErrorMessage = "Incorrect Username/Password";
                            }

                            break;
                        }
                        else if (EmailID != objAdmin.EmailID)
                        {
                            objAdminUsersModel.IsAuthenticated = false;
                            objAdminUsersModel.ErrorMessage = "User does not exist";
                        }
                    }
                }
                else
                {
                    objAdminUsersModel.IsAuthenticated = false;
                    objAdminUsersModel.ErrorMessage = "User does not exist";
                }
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return objAdminUsersModel;
        }

        public static AdminUsersModel GetAdminUserDetails(string AdminUserID)
        {
            FirebaseDB objFirebaseDB = null;
            AdminUsersModel objAdminUsersModel = null;

            try
            {
                objFirebaseDB = new FirebaseDB(Helper.FirebaseURL + "AdminUsers.json?orderBy=%22AdminUserID%22&equalTo=%22" + AdminUserID + "%22");

                string objFBResponse = objFirebaseDB.Get().JSONContent;

                var objAUserList = JsonConvert.DeserializeObject<IDictionary<string, AdminUsersModel>>(objFBResponse);

                if (objAUserList != null && objAUserList.Count() > 0)
                {
                    foreach (var key in objAUserList.Keys)
                    {
                        AdminUsersModel objFBAUser = (AdminUsersModel)objAUserList[key];

                        objAdminUsersModel = objFBAUser;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objFirebaseDB = null;
            }

            return objAdminUsersModel;
        }

        public static AdminUsersModel GetAdminUserDetailsByEmailID(string EmailID)
        {
            FirebaseDB objFirebaseDB = null;
            AdminUsersModel objAdminUsersModel = null;

            try
            {
                objFirebaseDB = new FirebaseDB(Helper.FirebaseURL + "AdminUsers.json?orderBy=%22EmailID%22&equalTo=%22" + EmailID + "%22");

                string objFBResponse = objFirebaseDB.Get().JSONContent;

                var objAUserList = JsonConvert.DeserializeObject<IDictionary<string, AdminUsersModel>>(objFBResponse);

                if (objAUserList != null && objAUserList.Count() > 0)
                {
                    foreach (var key in objAUserList.Keys)
                    {
                        AdminUsersModel objFBAUser = (AdminUsersModel)objAUserList[key];

                        objAdminUsersModel = objFBAUser;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objFirebaseDB = null;
            }

            return objAdminUsersModel;
        }

        public static bool UpdateAdminUser(AdminUsersModel model)
        {
            FirebaseDB objFirebaseDB = null;
            bool bResult = false;

            try
            {
                string companyURL = Helper.FirebaseURL + "AdminUsers.json";

                var aUserJson = JsonConvert.SerializeObject(new
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    EmailID = model.EmailID,
                    Password = model.Password,
                    Phone = model.Phone,
                    City = model.City
                });

                objFirebaseDB = new FirebaseDB(companyURL.Replace(".json", "") + "/" + model.AdminUserID + "/.json");

                HttpWebResponse objUpdateHttpResponse = objFirebaseDB.ExecuteWebRequest(new HttpMethod("PATCH").Method, aUserJson);

                if (objUpdateHttpResponse.StatusCode == HttpStatusCode.OK)
                    bResult = true;
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }

        public static bool ResetPassword(string adminUserID, string password)
        {
            FirebaseDB objFirebaseDB = null;
            bool bResult = false;

            try
            {
                string companyURL = Helper.FirebaseURL + "AdminUsers.json";

                var aUserJson = JsonConvert.SerializeObject(new
                {
                    Password = password
                });

                objFirebaseDB = new FirebaseDB(companyURL.Replace(".json", "") + "/" + adminUserID + "/.json");

                HttpWebResponse objUpdateHttpResponse = objFirebaseDB.ExecuteWebRequest(new HttpMethod("PATCH").Method, aUserJson);

                if (objUpdateHttpResponse.StatusCode == HttpStatusCode.OK)
                    bResult = true;
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }
    }
}
