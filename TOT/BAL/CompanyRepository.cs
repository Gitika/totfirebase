﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

using TOT.Models;
using TOT.DAL;
using System.Net.Http;
using System.Net;
using System.IO;
using System.Web.Mvc;

namespace TOT.BAL
{
    public class CompanyRepository
    {
        /*Company*/

        public static bool ActivateAccount(CompanyModel model)
        {
            FirebaseDB objFirebaseDB = null;
            bool bResult = false;

            try
            {
                string companyURL = Helper.FirebaseURL + "Users.json";

                var companyJson = JsonConvert.SerializeObject(new
                {
                    Password = model.Password,
                    Phone = model.Phone,
                    IsActive = true,
                    IsDeleted = false
                });

                objFirebaseDB = new FirebaseDB(companyURL.Replace(".json", "") + "/" + model.UserID + "/.json");

                HttpWebResponse objUpdateHttpResponse = objFirebaseDB.ExecuteWebRequest(new HttpMethod("PATCH").Method, companyJson);

                if (objUpdateHttpResponse.StatusCode == HttpStatusCode.OK)
                    bResult = true;
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }

        public static List<CompanyModel> GetCompanyList(string SearchText, int CurrentPage, int PageSize, out int totalRecordCount)
        {
            #region Commented Code
            //FirebaseDB objFirebaseDB1;

            //if (SearchText == "")
            //    objFirebaseDB1 = new FirebaseDB(Helper.FirebaseURL + "Company.json");
            //else
            //    objFirebaseDB1 = new FirebaseDB(Helper.FirebaseURL + "Company.json?orderBy=%22CompanyName%22&equalTo=%22" + SearchText + "%22");

            //List<CompanyModel> lstCompany = null;

            //try
            //{
            //    string objFBResponse1 = objFirebaseDB1.Get().JSONContent;

            //    var objCompanyList1 = Newtonsoft.Json.JsonConvert.DeserializeObject<IDictionary<string, CompanyModel>>(objFBResponse1);

            //    totalRecordCount = objCompanyList1.Count;

            //    int startIndex = 0;

            //    if (CurrentPage == 1)
            //    {
            //        startIndex = 1;
            //    }
            //    else
            //    {
            //        startIndex = ((CurrentPage - 1) * PageSize) + 1;
            //    }

            //    FirebaseDB objFirebaseDB;

            //    if (SearchText == "")
            //        objFirebaseDB = new FirebaseDB(Helper.FirebaseURL + "Company.json?orderBy=%22CID%22&startAt=" + startIndex + "&limitToFirst=" + PageSize);
            //    else
            //        objFirebaseDB = new FirebaseDB(Helper.FirebaseURL + "Company.json?orderBy=%22CompanyName%22&equalTo=%22" + SearchText + "%22");

            //    try
            //    {
            //        string objFBResponse = objFirebaseDB.Get().JSONContent;

            //        var objCompanyList = Newtonsoft.Json.JsonConvert.DeserializeObject<IDictionary<string, CompanyModel>>(objFBResponse);

            //        if (objCompanyList != null && objCompanyList.Count > 0)
            //        {
            //            lstCompany = new List<CompanyModel>();

            //            foreach (var CompanyKey in objCompanyList.Keys)
            //            {
            //                CompanyModel objFBCompany = (CompanyModel)objCompanyList[CompanyKey];
            //                CompanyModel objCompany = new Models.CompanyModel();

            //                objCompany.CompanyID = objFBCompany.CompanyID;
            //                objCompany.CompanyName = objFBCompany.CompanyName;
            //                objCompany.ContactName = objFBCompany.ContactName;
            //                objCompany.JobRole = objFBCompany.JobRole;
            //                objCompany.WorkEmail = objFBCompany.WorkEmail;
            //                objCompany.Password = objFBCompany.Password;
            //                objCompany.Phone = objFBCompany.Phone;
            //                objCompany.City = objFBCompany.City;
            //                objCompany.Country = objFBCompany.Country;
            //                objCompany.IsActive = objFBCompany.IsActive;
            //                objCompany.IsDeleted = objFBCompany.IsDeleted;
            //                objCompany.DateCreated = objFBCompany.DateCreated;
            //                objCompany.DateUpdated = objFBCompany.DateUpdated;

            //                lstCompany.Add(objCompany);
            //            }
            //        }
            //    }
            //    catch (System.Threading.ThreadAbortException ex1)
            //    {
            //    }
            //    catch (Exception ex)
            //    {
            //        //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            //    }
            //    finally
            //    {
            //        objFirebaseDB = null;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    totalRecordCount = -1;
            //}

            //return lstCompany;
            #endregion

            FirebaseDB objFirebaseDB = new FirebaseDB(Helper.FirebaseURL + "Users.json?orderBy=%22UserType%22&equalTo=%22Company%22");
            List<CompanyModel> lstCompany = null;

            try
            {
                string objFBResponse = objFirebaseDB.Get().JSONContent;

                var objCompanyList = Newtonsoft.Json.JsonConvert.DeserializeObject<IDictionary<string, CompanyModel>>(objFBResponse);

                totalRecordCount = 0;

                if (objCompanyList != null && objCompanyList.Count() > 0)
                {
                    var result = objCompanyList.Where(x => x.Value.IsDeleted == false).Where(x => x.Value.FirstName.ToLower().Contains(SearchText.ToLower())).Skip((CurrentPage - 1) * PageSize).Take(PageSize).ToList();
                    var totalRecords = objCompanyList.Where(x => x.Value.IsDeleted == false).Where(x => x.Value.FirstName.ToLower().Contains(SearchText.ToLower())).ToList();

                    lstCompany = new List<CompanyModel>();

                    totalRecordCount = totalRecords.Count();

                    for (int i = 0; i < result.Count(); i++)
                    {
                        CompanyModel objFBCompany = result[i].Value;
                        CompanyModel objCompany = new CompanyModel();

                        objCompany = objFBCompany;

                        lstCompany.Add(objCompany);
                    }
                }
            }
            catch (Exception ex)
            {
                totalRecordCount = 0;
            }
            finally
            {
                objFirebaseDB = null;
            }

            return lstCompany;
        }

        public static List<SelectListItem> GetCountryList()
        {
            FirebaseDB objFirebaseDB;

            objFirebaseDB = new FirebaseDB(Helper.FirebaseURL + "Country.json");

            List<SelectListItem> lstCountry = null;

            try
            {
                string objFBResponse = objFirebaseDB.Get().JSONContent;

                var objCountryList = Newtonsoft.Json.JsonConvert.DeserializeObject<IDictionary<string, CountryModel>>(objFBResponse);

                if (objCountryList != null && objCountryList.Count > 0)
                {
                    lstCountry = new List<SelectListItem>();

                    foreach (var key in objCountryList.Keys)
                    {
                        CountryModel objFBCountry = (CountryModel)objCountryList[key];
                        CountryModel objCountry = new CountryModel();

                        objCountry.CountryID = objFBCountry.CountryID;
                        objCountry.CountryName = objFBCountry.CountryName;
                        objCountry.IsActive = objFBCountry.IsActive;
                        objCountry.IsDeleted = objFBCountry.IsDeleted;

                        lstCountry.Add(new SelectListItem { Text = objFBCountry.CountryName, Value = objFBCountry.CountryID.ToString() });
                    }
                }
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return lstCountry;
        }

        public static bool IsCompanyExist(string UserID, string EmailID)
        {
            FirebaseDB objFirebaseDB = new FirebaseDB(Helper.FirebaseURL + "Users.json?orderBy=%22UserType%22&equalTo=%22" + Helper.UserType.Company.ToString() + "%22");
            bool bResult = false;

            try
            {
                string objFBResponse = objFirebaseDB.Get().JSONContent;

                var objCompanyList = JsonConvert.DeserializeObject<IDictionary<string, CompanyModel>>(objFBResponse);

                if (objCompanyList != null && objCompanyList.Count > 0)
                {
                    foreach (var key in objCompanyList.Keys)
                    {
                        CompanyModel objFBCompany = (CompanyModel)objCompanyList[key];

                        if (UserID == "")//Add mode
                        {
                            if (objFBCompany.EmailID == EmailID && !objFBCompany.IsDeleted)
                                bResult = true;
                        }
                        else//Edit mode
                        {
                            if (objFBCompany.EmailID == EmailID && !objFBCompany.IsDeleted && objFBCompany.UserID != UserID)
                                bResult = true;
                        }
                    }
                }
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }

        public static bool InsertCompany(CompanyModel model, out string userID)
        {
            FirebaseDB objFirebaseDB = null;
            bool bResult = false;

            try
            {
                string companyURL = Helper.FirebaseURL + "Users.json";

                string companyJson = JsonConvert.SerializeObject(model);

                objFirebaseDB = new FirebaseDB(companyURL);

                userID = "";

                if (string.IsNullOrEmpty(model.UserID))
                {
                    HttpWebResponse objInsertHttpResponse = objFirebaseDB.ExecuteWebRequest(HttpMethod.Post.Method, companyJson);

                    if (objInsertHttpResponse != null)
                    {
                        if (objInsertHttpResponse.StatusCode == HttpStatusCode.OK)
                        {
                            string objInsertResponse = (new StreamReader(objInsertHttpResponse.GetResponseStream())).ReadToEnd();

                            Dictionary<string, object> objFBResponse = JsonConvert.DeserializeObject<Dictionary<string, object>>(objInsertResponse);

                            if (objFBResponse != null)
                            {
                                FirebaseDB objFirebaseDB1 = new FirebaseDB(companyURL.Replace(".json", "") + "/" + objFBResponse["name"].ToString() + "/.json");

                                var json1 = JsonConvert.SerializeObject(new
                                {
                                    UserID = objFBResponse["name"].ToString()
                                });

                                HttpWebResponse objUpdateHttpResponse = objFirebaseDB1.ExecuteWebRequest(new HttpMethod("PATCH").Method, json1);

                                if (objUpdateHttpResponse.StatusCode == HttpStatusCode.OK)
                                {
                                    bResult = true;
                                    userID = objFBResponse["name"].ToString();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                userID = "";
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }

        public static bool InsertCompanyWithKey(CompanyModel model, string userUID)
        {
            FirebaseDB objFirebaseDB = null;
            bool bResult = false;

            try
            {
                string companyURL = Helper.FirebaseURL + "Users/" + userUID + "/.json";

                string companyJson = JsonConvert.SerializeObject(model);

                objFirebaseDB = new FirebaseDB(companyURL);

                HttpWebResponse objInsertHttpResponse = objFirebaseDB.ExecuteWebRequest(HttpMethod.Put.Method, companyJson);

                if (objInsertHttpResponse != null)
                {
                    if (objInsertHttpResponse.StatusCode == HttpStatusCode.OK)
                    {
                        bResult = true;

                        //string objInsertResponse = (new StreamReader(objInsertHttpResponse.GetResponseStream())).ReadToEnd();

                        //Dictionary<string, object> objFBResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(objInsertResponse);

                        //if (objFBResponse != null)
                        //{
                        //    FirebaseDB objFirebaseDB1 = new FirebaseDB(buyerURL);

                        //    var json1 = JsonConvert.SerializeObject(new
                        //    {
                        //        UserID = objFBResponse["name"].ToString()
                        //    });

                        //    HttpWebResponse objUpdateHttpResponse = objFirebaseDB1.ExecuteWebRequest(new HttpMethod("PATCH").Method, json1);

                        //    if (objUpdateHttpResponse.StatusCode == HttpStatusCode.OK)
                        //    {
                        //        bResult = true;
                        //        buyerID = objFBResponse["name"].ToString();
                        //    }
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }

        public static CompanyModel GetCompanyDetails(string UserID)
        {
            FirebaseDB objFirebaseDB = null;
            CompanyModel objCompanyModel = null;

            try
            {
                objFirebaseDB = new FirebaseDB(Helper.FirebaseURL + "Users.json?orderBy=%22UserID%22&equalTo=%22" + UserID + "%22");
                //objFirebaseDB = new FirebaseDB(Helper.FirebaseURL + "Company.json?orderBy=%22CompanyID%22&equalTo=%22" + CompanyID + "%22");

                string objFBResponse = objFirebaseDB.Get().JSONContent;

                var objCompanyList = JsonConvert.DeserializeObject<IDictionary<string, CompanyModel>>(objFBResponse);

                if (objCompanyList != null && objCompanyList.Count() > 0)
                {
                    foreach (var key in objCompanyList.Keys)
                    {
                        CompanyModel objFBCompany = (CompanyModel)objCompanyList[key];

                        objCompanyModel = objFBCompany;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objFirebaseDB = null;
            }

            return objCompanyModel;
        }

        public static CompanyModel GetCompanyDetailsByEmailID(string EmailID)
        {
            FirebaseDB objFirebaseDB = null;
            CompanyModel objCompanyModel = null;

            try
            {
                objFirebaseDB = new FirebaseDB(Helper.FirebaseURL + "Users.json?orderBy=%22UserType%22&equalTo=%22" + Helper.UserType.Company.ToString() + "%22");
                //objFirebaseDB = new FirebaseDB(Helper.FirebaseURL + "Users.json?orderBy=%22EmailID%22&equalTo=%22" + EmailID + "%22");

                string objFBResponse = objFirebaseDB.Get().JSONContent;

                var objCompanyList = JsonConvert.DeserializeObject<IDictionary<string, CompanyModel>>(objFBResponse);

                if (objCompanyList != null && objCompanyList.Count() > 0)
                {
                    foreach (var key in objCompanyList.Keys)
                    {
                        CompanyModel objFBCompany = (CompanyModel)objCompanyList[key];

                        if (objFBCompany.EmailID == EmailID && !objFBCompany.IsDeleted)
                        {
                            objCompanyModel = objFBCompany;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objFirebaseDB = null;
            }

            return objCompanyModel;
        }

        public static bool UpdateCompany(CompanyModel model)
        {
            FirebaseDB objFirebaseDB = null;
            bool bResult = false;

            try
            {
                string companyURL = Helper.FirebaseURL + "Users.json";

                var companyJson = JsonConvert.SerializeObject(new
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    ContactName = model.FirstName + " " + model.LastName,
                    CompanyName = model.CompanyName,
                    JobTitle = model.JobTitle,
                    Phone = model.Phone,
                    EmailID = model.EmailID,
                    City = model.City,
                    CountryID = model.CountryID,
                    DateUpdated = DateTime.Now.ToLocalTime().ToString()
                });

                objFirebaseDB = new FirebaseDB(companyURL.Replace(".json", "") + "/" + model.UserID + "/.json");

                HttpWebResponse objUpdateHttpResponse = objFirebaseDB.ExecuteWebRequest(new HttpMethod("PATCH").Method, companyJson);

                if (objUpdateHttpResponse.StatusCode == HttpStatusCode.OK)
                    bResult = true;
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }

        public static bool DeleteCompany(string UserID)
        {
            FirebaseDB objFirebaseDB = null;
            bool bResult = false;

            try
            {
                string buyerURL = Helper.FirebaseURL + "Users.json";

                var companyJson = JsonConvert.SerializeObject(new
                {
                    UserID = UserID,
                    IsDeleted = true
                });

                objFirebaseDB = new FirebaseDB(buyerURL.Replace(".json", "") + "/" + UserID + "/.json");

                HttpWebResponse objUpdateHttpResponse = objFirebaseDB.ExecuteWebRequest(new HttpMethod("PATCH").Method, companyJson);

                if (objUpdateHttpResponse.StatusCode == HttpStatusCode.OK)
                    bResult = true;
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }

        public static bool DeleteCompanyPermanently(string userId)
        {
            FirebaseDB objFirebaseDB = null;
            bool bResult = false;

            try
            {
                string companyURL = Helper.FirebaseURL + "Users.json";

                var companyJson = JsonConvert.SerializeObject(new
                {
                    UserID = userId
                });

                objFirebaseDB = new FirebaseDB(companyURL.Replace(".json", "") + "/" + userId + "/.json");

                HttpWebResponse objUpdateHttpResponse = objFirebaseDB.ExecuteWebRequest(HttpMethod.Delete.Method, companyJson);  // ExecuteWebRequest(new HttpMethod("PATCH").Method, buyerJson);

                if (objUpdateHttpResponse.StatusCode == HttpStatusCode.OK)
                    bResult = true;
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }

        public static bool ResetPassword(string userID, string password)
        {
            FirebaseDB objFirebaseDB = null;
            bool bResult = false;

            try
            {
                string companyURL = Helper.FirebaseURL + "Users.json";

                var companyJson = JsonConvert.SerializeObject(new
                {
                    Password = password,
                });

                objFirebaseDB = new FirebaseDB(companyURL.Replace(".json", "") + "/" + userID + "/.json");

                HttpWebResponse objUpdateHttpResponse = objFirebaseDB.ExecuteWebRequest(new HttpMethod("PATCH").Method, companyJson);

                if (objUpdateHttpResponse.StatusCode == HttpStatusCode.OK)
                    bResult = true;
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }

        public static CompanyModel Authenticate(string EmailID, string Password)
        {
            FirebaseDB objFirebaseDB = new FirebaseDB(Helper.FirebaseURL + "Users.json");
            CompanyModel objCompanyModel = new CompanyModel();

            try
            {
                string objFBResponse = objFirebaseDB.Get().JSONContent;

                var objJavaScriptSerializer = new JavaScriptSerializer();

                //objAdminUsersModel = Newtonsoft.Json.JsonConvert.DeserializeObject<AdminUsersModel>(objFBResponse);
                //AdminUserList objAdminUserList = Newtonsoft.Json.JsonConvert.DeserializeObject<AdminUserList>(objFBResponse);

                var objCompanyModelList = objJavaScriptSerializer.Deserialize<IDictionary<string, CompanyModel>>(objFBResponse);

                if (objCompanyModelList != null && objCompanyModelList.Count > 0)
                {
                    foreach (var AUser in objCompanyModelList.Keys)
                    {
                        CompanyModel objCompany = objCompanyModelList[AUser];

                        if (objCompany.UserType == "Company")
                        {
                            if (EmailID == objCompany.EmailID)
                            {
                                if (Password == objCompany.Password)
                                {
                                    objCompanyModel = objCompany;
                                    objCompanyModel.IsAuthenticated = true;
                                }
                                else
                                {
                                    objCompanyModel.IsAuthenticated = false;
                                    objCompanyModel.ErrorMessage = "Incorrect Username/Password";
                                }

                                break;
                            }
                            else if (EmailID != objCompany.EmailID)
                            {
                                objCompanyModel.IsAuthenticated = false;
                                objCompanyModel.ErrorMessage = "User does not exist";
                            }
                        }
                        else
                        {
                            objCompanyModel.IsAuthenticated = false;
                            objCompanyModel.ErrorMessage = "User does not exist";
                        }
                    }
                }
                else
                {
                    objCompanyModel.IsAuthenticated = false;
                    objCompanyModel.ErrorMessage = "User does not exist";
                }
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return objCompanyModel;
        }

        /*Buyer*/

        public static bool ActivateBuyer(BuyerModel model)
        {
            FirebaseDB objFirebaseDB = null;
            bool bResult = false;

            try
            {
                string buyerURL = Helper.FirebaseURL + "Users.json";

                var buyerJson = JsonConvert.SerializeObject(new
                {
                    Password = model.Password,
                    Phone = model.Phone,
                    IsActive = true,
                    IsDeleted = false
                });

                objFirebaseDB = new FirebaseDB(buyerURL.Replace(".json", "") + "/" + model.UserID + "/.json");

                HttpWebResponse objUpdateHttpResponse = objFirebaseDB.ExecuteWebRequest(new HttpMethod("PATCH").Method, buyerJson);

                if (objUpdateHttpResponse.StatusCode == HttpStatusCode.OK)
                    bResult = true;
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }

        public static int GetBuyersCount(string searchText)
        {
            try
            {
                FirebaseDB objFirebaseDB = new FirebaseDB(Helper.FirebaseURL + "Users.json?orderBy=\"UserType\"&equalTo=\"" + Helper.UserType.Buyer + "\"");
                string objFBResponse = objFirebaseDB.Get().JSONContent;
                var objFullBuyersList = Newtonsoft.Json.JsonConvert.DeserializeObject<IDictionary<string, BuyerModel>>(objFBResponse);
                if (objFullBuyersList != null)
                {
                    return objFullBuyersList.Count;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<BuyerModel> GetBuyersList(string CompanyID, string searchText, int currentPage, int pageSize, out int totalCount)
        {
            FirebaseDB objFirebaseDB = new FirebaseDB(Helper.FirebaseURL + "Users.json?orderBy=\"UserType\"&equalTo=\"" + Helper.UserType.Buyer + "\"");
            List<BuyerModel> lstBuyers = null;

            try
            {
                string objFBResponse = objFirebaseDB.Get().JSONContent;

                var objBuyersList = Newtonsoft.Json.JsonConvert.DeserializeObject<IDictionary<string, BuyerModel>>(objFBResponse);

                if (objBuyersList != null && objBuyersList.Count() > 0)
                {
                    var result = objBuyersList.Where(x => x.Value.CompanyID == CompanyID).Where(x => x.Value.IsDeleted == false).Where(x => x.Value.FirstName.ToLower().Contains(searchText.ToLower())).Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                    var totalRecords = objBuyersList.Where(x => x.Value.CompanyID == CompanyID).Where(x => x.Value.IsDeleted == false).Where(x => x.Value.FirstName.ToLower().Contains(searchText.ToLower())).ToList();

                    lstBuyers = new List<BuyerModel>();

                    totalCount = totalRecords.Count();

                    for (int i = 0; i < result.Count(); i++)
                    {
                        BuyerModel objFBBuyer = result[i].Value;
                        BuyerModel objBuyer = new Models.BuyerModel();

                        objBuyer = objFBBuyer;

                        lstBuyers.Add(objBuyer);
                    }
                }
                else
                    totalCount = 0;
            }
            catch (Exception ex)
            {
                totalCount = 0;
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }

            return lstBuyers;
        }

        public static bool IsBuyerExist(string userID, string CompanyID, string EmailID)
        {
            FirebaseDB objFirebaseDB = new FirebaseDB(Helper.FirebaseURL + "Users.json?orderBy=%22UserType%22&equalTo=%22" + Helper.UserType.Buyer.ToString() + "%22");
            bool bResult = false;

            try
            {
                string objFBResponse = objFirebaseDB.Get().JSONContent;

                var objBuyerList = JsonConvert.DeserializeObject<IDictionary<string, BuyerModel>>(objFBResponse);

                if (objBuyerList != null && objBuyerList.Count > 0)
                {
                    foreach (var key in objBuyerList.Keys)
                    {
                        BuyerModel objFBBuyer = (BuyerModel)objBuyerList[key];

                        if (userID == "")//Add mode
                        {
                            if (objFBBuyer.EmailID == EmailID && objFBBuyer.CompanyID == CompanyID && !objFBBuyer.IsDeleted)
                                bResult = true;
                        }
                        else//Edit mode
                        {
                            if (objFBBuyer.EmailID == EmailID && objFBBuyer.CompanyID == CompanyID && !objFBBuyer.IsDeleted && objFBBuyer.UserID != userID)
                                bResult = true;
                        }
                    }
                }
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }

        public static bool InsertBuyer(BuyerModel model, out string buyerID)
        {
            FirebaseDB objFirebaseDB = null;
            bool bResult = false;

            try
            {
                string buyerURL = Helper.FirebaseURL + "Users.json";

                string buyerJson = JsonConvert.SerializeObject(model);

                objFirebaseDB = new FirebaseDB(buyerURL);

                buyerID = "";

                if (string.IsNullOrEmpty(model.UserID))
                {
                    HttpWebResponse objInsertHttpResponse = objFirebaseDB.ExecuteWebRequest(HttpMethod.Post.Method, buyerJson);

                    if (objInsertHttpResponse != null)
                    {
                        if (objInsertHttpResponse.StatusCode == HttpStatusCode.OK)
                        {
                            string objInsertResponse = (new StreamReader(objInsertHttpResponse.GetResponseStream())).ReadToEnd();

                            Dictionary<string, object> objFBResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(objInsertResponse);

                            if (objFBResponse != null)
                            {
                                FirebaseDB objFirebaseDB1 = new FirebaseDB(buyerURL.Replace(".json", "") + "/" + objFBResponse["name"].ToString() + "/.json");

                                var json1 = JsonConvert.SerializeObject(new
                                {
                                    UserID = objFBResponse["name"].ToString()
                                });

                                HttpWebResponse objUpdateHttpResponse = objFirebaseDB1.ExecuteWebRequest(new HttpMethod("PATCH").Method, json1);

                                if (objUpdateHttpResponse.StatusCode == HttpStatusCode.OK)
                                {
                                    bResult = true;
                                    buyerID = objFBResponse["name"].ToString();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                buyerID = "";
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }

        public static bool InsertBuyerWithKey(BuyerModel model, string userUID)
        {
            FirebaseDB objFirebaseDB = null;
            bool bResult = false;

            try
            {
                string buyerURL = Helper.FirebaseURL + "Users/" + userUID + "/.json";

                string buyerJson = JsonConvert.SerializeObject(model);

                objFirebaseDB = new FirebaseDB(buyerURL);

                HttpWebResponse objInsertHttpResponse = objFirebaseDB.ExecuteWebRequest(HttpMethod.Put.Method, buyerJson);

                if (objInsertHttpResponse != null)
                {
                    if (objInsertHttpResponse.StatusCode == HttpStatusCode.OK)
                    {
                        bResult = true;

                        //string objInsertResponse = (new StreamReader(objInsertHttpResponse.GetResponseStream())).ReadToEnd();

                        //Dictionary<string, object> objFBResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(objInsertResponse);

                        //if (objFBResponse != null)
                        //{
                        //    FirebaseDB objFirebaseDB1 = new FirebaseDB(buyerURL);

                        //    var json1 = JsonConvert.SerializeObject(new
                        //    {
                        //        UserID = objFBResponse["name"].ToString()
                        //    });

                        //    HttpWebResponse objUpdateHttpResponse = objFirebaseDB1.ExecuteWebRequest(new HttpMethod("PATCH").Method, json1);

                        //    if (objUpdateHttpResponse.StatusCode == HttpStatusCode.OK)
                        //    {
                        //        bResult = true;
                        //        buyerID = objFBResponse["name"].ToString();
                        //    }
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }

        public static BuyerModel GetBuyerDetail(string userId)
        {
            FirebaseDB objFirebaseDB = null;
            BuyerModel objBuyerModel = null;

            try
            {
                objFirebaseDB = new FirebaseDB(Helper.FirebaseURL + "Users.json?orderBy=%22UserID%22&equalTo=%22" + userId + "%22");

                string objFBResponse = objFirebaseDB.Get().JSONContent;

                var objBuyersList = JsonConvert.DeserializeObject<IDictionary<string, BuyerModel>>(objFBResponse);

                if (objBuyersList != null && objBuyersList.Count() > 0)
                {
                    foreach (var key in objBuyersList.Keys)
                    {
                        BuyerModel objFBBuyer = (BuyerModel)objBuyersList[key];

                        objBuyerModel = objFBBuyer;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objFirebaseDB = null;
            }

            return objBuyerModel;
        }

        public static bool UpdateBuyer(BuyerModel model)
        {
            FirebaseDB objFirebaseDB = null;
            bool bResult = false;

            try
            {
                string buyerURL = Helper.FirebaseURL + "Users.json";

                var buyerJson = JsonConvert.SerializeObject(new
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    ContactName = model.FirstName + " " + model.LastName,
                    JobTitle = model.JobTitle,
                    Phone = model.Phone,
                    EmailID = model.EmailID,
                    City = model.City,
                    IsActive = model.IsActive,
                    DateUpdated = DateTime.Now.ToLocalTime().ToString()
                });

                objFirebaseDB = new FirebaseDB(buyerURL.Replace(".json", "") + "/" + model.UserID + "/.json");

                HttpWebResponse objUpdateHttpResponse = objFirebaseDB.ExecuteWebRequest(new HttpMethod("PATCH").Method, buyerJson);

                if (objUpdateHttpResponse.StatusCode == HttpStatusCode.OK)
                    bResult = true;
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }

        public static bool DeleteBuyer(string userId)
        {
            FirebaseDB objFirebaseDB = null;
            bool bResult = false;

            try
            {
                string buyerURL = Helper.FirebaseURL + "Users.json";

                var buyerJson = JsonConvert.SerializeObject(new
                {
                    UserID = userId,
                    IsDeleted = true
                });

                objFirebaseDB = new FirebaseDB(buyerURL.Replace(".json", "") + "/" + userId + "/.json");

                HttpWebResponse objUpdateHttpResponse = objFirebaseDB.ExecuteWebRequest(new HttpMethod("PATCH").Method, buyerJson);

                if (objUpdateHttpResponse.StatusCode == HttpStatusCode.OK)
                    bResult = true;
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }

        public static bool DeleteBuyerPermanently(string userId)
        {
            FirebaseDB objFirebaseDB = null;
            bool bResult = false;

            try
            {
                string buyerURL = Helper.FirebaseURL + "Users.json";

                var buyerJson = JsonConvert.SerializeObject(new
                {
                    UserID = userId
                });

                objFirebaseDB = new FirebaseDB(buyerURL.Replace(".json", "") + "/" + userId + "/.json");

                HttpWebResponse objUpdateHttpResponse = objFirebaseDB.ExecuteWebRequest(HttpMethod.Delete.Method, buyerJson);  // ExecuteWebRequest(new HttpMethod("PATCH").Method, buyerJson);

                if (objUpdateHttpResponse.StatusCode == HttpStatusCode.OK)
                    bResult = true;
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }

        /*Seasons*/

        public static List<SeasonsModel> GetSeasonsList(string UserID, string searchText, int currentPage, int pageSize, out int totalCount)
        {
            FirebaseDB objFirebaseDB = new FirebaseDB(Helper.FirebaseURL + "TrendsSeason.json?orderBy=%22Name%22");
            List<SeasonsModel> lstSeasons = null;

            try
            {
                string objFBResponse = objFirebaseDB.Get().JSONContent;

                var objSeasonList = JsonConvert.DeserializeObject<IDictionary<string, SeasonsModel>>(objFBResponse);

                if (objSeasonList != null && objSeasonList.Count() > 0)
                {
                    var result = objSeasonList.Where(x => x.Value.UserID == UserID).Where(x => x.Value.IsDeleted == false).Where(x => x.Value.Name.ToLower().Contains(searchText.ToLower())).Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                    var totalRecords = objSeasonList.Where(x => x.Value.UserID == UserID).Where(x => x.Value.IsDeleted == false).Where(x => x.Value.Name.ToLower().Contains(searchText.ToLower())).ToList();

                    lstSeasons = new List<SeasonsModel>();

                    totalCount = totalRecords.Count();

                    for (int i = 0; i < result.Count(); i++)
                    {
                        SeasonsModel objFBSeasons = result[i].Value;
                        SeasonsModel objSeasons = new Models.SeasonsModel();

                        objSeasons = objFBSeasons;

                        lstSeasons.Add(objSeasons);
                    }
                }
                else
                    totalCount = 0;
            }
            catch (Exception ex)
            {
                totalCount = 0;
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }

            return lstSeasons;
        }

        public static bool IsSeasonExist(string SeasonID, string UserID, string SeasonName)
        {
            FirebaseDB objFirebaseDB = new FirebaseDB(Helper.FirebaseURL + "TrendsSeason.json");
            bool bResult = false;

            try
            {
                string objFBResponse = objFirebaseDB.Get().JSONContent;

                var objSeasonList = JsonConvert.DeserializeObject<IDictionary<string, SeasonsModel>>(objFBResponse);

                if (objSeasonList != null && objSeasonList.Count > 0)
                {
                    foreach (var key in objSeasonList.Keys)
                    {
                        SeasonsModel objFBSeason = (SeasonsModel)objSeasonList[key];

                        if (SeasonID == "")//Add mode
                        {
                            if (objFBSeason.Name == SeasonName && objFBSeason.UserID == UserID && !objFBSeason.IsDeleted)
                                bResult = true;
                        }
                        else//Edit mode
                        {
                            if (objFBSeason.Name == SeasonName && objFBSeason.UserID == UserID && !objFBSeason.IsDeleted && objFBSeason.ID != SeasonID)
                                bResult = true;
                        }
                    }
                }
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }

        public static bool InsertSeason(SeasonsModel model)
        {
            FirebaseDB objFirebaseDB = null;
            bool bResult = false;

            try
            {
                string seasonURL = Helper.FirebaseURL + "TrendsSeason.json";

                string seasonJson = JsonConvert.SerializeObject(model);

                objFirebaseDB = new FirebaseDB(seasonURL);

                if (string.IsNullOrEmpty(model.ID))
                {
                    HttpWebResponse objInsertHttpResponse = objFirebaseDB.ExecuteWebRequest(HttpMethod.Post.Method, seasonJson);

                    if (objInsertHttpResponse != null)
                    {
                        if (objInsertHttpResponse.StatusCode == HttpStatusCode.OK)
                        {
                            string objInsertResponse = (new StreamReader(objInsertHttpResponse.GetResponseStream())).ReadToEnd();

                            Dictionary<string, object> objFBResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(objInsertResponse);

                            if (objFBResponse != null)
                            {
                                FirebaseDB objFirebaseDB1 = new FirebaseDB(seasonURL.Replace(".json", "") + "/" + objFBResponse["name"].ToString() + "/.json");

                                var json1 = JsonConvert.SerializeObject(new
                                {
                                    ID = objFBResponse["name"].ToString()
                                });

                                HttpWebResponse objUpdateHttpResponse = objFirebaseDB1.ExecuteWebRequest(new HttpMethod("PATCH").Method, json1);

                                if (objUpdateHttpResponse.StatusCode == HttpStatusCode.OK)
                                    bResult = true;
                            }
                        }
                    }
                }
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }

        public static SeasonsModel GetSeasonDetail(string SeasonID)
        {
            FirebaseDB objFirebaseDB = null;
            SeasonsModel objSeasonModel = null;

            try
            {
                objFirebaseDB = new FirebaseDB(Helper.FirebaseURL + "TrendsSeason.json?orderBy=%22ID%22&equalTo=%22" + SeasonID + "%22");

                string objFBResponse = objFirebaseDB.Get().JSONContent;

                var objSeasonList = JsonConvert.DeserializeObject<IDictionary<string, SeasonsModel>>(objFBResponse);

                if (objSeasonList != null && objSeasonList.Count() > 0)
                {
                    foreach (var key in objSeasonList.Keys)
                    {
                        SeasonsModel objFBSeason = (SeasonsModel)objSeasonList[key];

                        objSeasonModel = objFBSeason;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objFirebaseDB = null;
            }

            return objSeasonModel;
        }

        public static bool UpdateSeason(SeasonsModel model)
        {
            FirebaseDB objFirebaseDB = null;
            bool bResult = false;

            try
            {
                string buyerURL = Helper.FirebaseURL + "TrendsSeason.json";

                var buyerJson = JsonConvert.SerializeObject(new
                {
                    Name = model.Name,
                    IsActive = model.IsActive
                });

                objFirebaseDB = new FirebaseDB(buyerURL.Replace(".json", "") + "/" + model.ID + "/.json");

                HttpWebResponse objUpdateHttpResponse = objFirebaseDB.ExecuteWebRequest(new HttpMethod("PATCH").Method, buyerJson);

                if (objUpdateHttpResponse.StatusCode == HttpStatusCode.OK)
                    bResult = true;
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }

        public static bool DeleteSeason(string SeasonID)
        {
            FirebaseDB objFirebaseDB = null;
            bool bResult = false;

            try
            {
                string buyerURL = Helper.FirebaseURL + "TrendsSeason.json";

                var buyerJson = JsonConvert.SerializeObject(new
                {
                    ID = SeasonID,
                    isDeleted = true
                });

                objFirebaseDB = new FirebaseDB(buyerURL.Replace(".json", "") + "/" + SeasonID + "/.json");

                HttpWebResponse objUpdateHttpResponse = objFirebaseDB.ExecuteWebRequest(new HttpMethod("PATCH").Method, buyerJson);

                if (objUpdateHttpResponse.StatusCode == HttpStatusCode.OK)
                    bResult = true;
            }
            catch (System.Threading.ThreadAbortException ex1)
            {
            }
            catch (Exception ex)
            {
                //Helper.SendExceptionMail("GetDetails.cs", "GetAdminUser", ex.Message + "----" + ex.StackTrace);
            }
            finally
            {
                objFirebaseDB = null;
            }

            return bResult;
        }
    }
}