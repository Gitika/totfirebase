﻿using System;
using System.Web;
using System.Configuration;
using System.Net.Mail;
using System.Xml;
using System.Net;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace TOT.BAL
{
    public class Helper
    {
        public enum UserType
        {
            Company,
            Buyer,
            Supplier
        }

        public static string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["MainConnection"].ConnectionString;
            }
        }

        public static string AppPhysicalPath
        {
            get
            {
                return ConfigurationManager.AppSettings["AppPhysicalPath"].ToString();
            }
        }

        public static string SiteURL
        {
            get
            {
                return ConfigurationManager.AppSettings["SiteURL"].ToString();
            }
        }

        public static string AdminCMSURL
        {
            get
            {
                return ConfigurationManager.AppSettings["AdminCMSURL"].ToString();
            }
        }

        public static string CompanyCMSURL
        {
            get
            {
                return ConfigurationManager.AppSettings["CompanyCMSURL"].ToString();
            }
        }

        public static string FirebaseURL
        {
            get
            {
                return ConfigurationManager.AppSettings["FirebaseURL"].ToString();
            }
        }

        public static string ReadEmailTemplate(string path)
        {
            string fileContent = string.Empty;
            if (File.Exists(path))
                fileContent = File.ReadAllText(path);
            return fileContent;
        }

        public static void SendMail(string to_name, string from_name, string subject, string body, string CC = "***", string bcc = "***")
        {
            SmtpClient smtp = new SmtpClient();

            try
            {
                MailMessage mm = new MailMessage(from_name, to_name);
                mm.Subject = subject;
                string vbody = "";
                vbody = body;
                mm.Body = vbody;
                if (CC != "***")
                    mm.CC.Add(CC);
                mm.IsBodyHtml = true;
                smtp.Host = ConfigurationManager.AppSettings["SMTP"].ToString();
                smtp.EnableSsl = true;
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"].ToString());
                smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUser"].ToString(), ConfigurationManager.AppSettings["SMTPPass"].ToString());
                smtp.Send(mm);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                smtp.Dispose();
            }
        }

        public static void SendExceptionMail(string ContollerName, string Action, string ExceptionMsg, string CC = "***", string bcc = "***")
        {
            try
            {
                string strBody = "<font face='arial' size='2'><br>Hello, <br><br></font>";
                strBody = strBody + "<font face='arial' size='2'>";
                strBody = strBody + "<br>There is exception in Controller:<b>" + ContollerName + "</b>";
                strBody = strBody + "<br>Action:<b>" + Action + "</b>";
                strBody = strBody + "<br>Error Message:<b><br />" + ExceptionMsg.Replace("----", "<br /><br />") + "</b>";
                strBody = strBody + "<br><br>Sincerely";
                strBody = strBody + "<br><br>" + ConfigurationManager.AppSettings["CoName"] + " Team</font>";

                MailMessage mm = new MailMessage(ConfigurationManager.AppSettings["FromMail"].ToString(), ConfigurationManager.AppSettings["AdminMail"].ToString());
                mm.Subject = "Error in Controller: " + ContollerName;
                string vbody = "";
                vbody = strBody;
                mm.Body = vbody;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["SMTP"].ToString();
                smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUser"].ToString(), ConfigurationManager.AppSettings["SMTPPass"].ToString());
                smtp.Send(mm);
            }
            catch (Exception ex)
            {
            }
        }

        public static string GetLongitudeAndLatitude(string address, string sensor)
        {
            string urlAddress = "https://maps.googleapis.com/maps/api/geocode/xml?address=" + HttpUtility.UrlEncode(address) + "&sensor=" + sensor;
            string returnValue = "";
            try
            {
                XmlDocument objXmlDocument = new XmlDocument();
                objXmlDocument.Load(urlAddress);
                XmlNodeList objXmlNodeList = objXmlDocument.SelectNodes("/GeocodeResponse/result/geometry/location");
                foreach (XmlNode objXmlNode in objXmlNodeList)
                {
                    //LATITUDE 
                    returnValue = objXmlNode.ChildNodes.Item(0).InnerText;

                    //LONGITUDE 
                    returnValue += "," + objXmlNode.ChildNodes.Item(1).InnerText;
                }
            }
            catch (Exception ex)
            {
                Helper.SendExceptionMail("Helper.cs", "GetLongitudeAndLatitude", ex.Message + "----" + ex.StackTrace);
            }

            return returnValue;
        }

        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public static string Decrypt(string cipherText)
        {
            try
            {
                string EncryptionKey = "MAKV2SPBNI99212";
                cipherText = cipherText.Replace(" ", "+");
                byte[] cipherBytes = Convert.FromBase64String(cipherText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        cipherText = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
                return cipherText;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string MakeTinyUrl(string Url)
        {
            try
            {
                if (Url.Length <= 30)
                {
                    return Url;
                }
                if (!Url.ToLower().StartsWith("http") && !Url.ToLower().StartsWith("ftp"))
                {
                    Url = "https://" + Url;
                }

                WebRequest request = WebRequest.Create("https://tinyurl.com/api-create.php?url=" + Url);
                WebResponse res = request.GetResponse();

                string text = null;
                using (StreamReader reader = new StreamReader(res.GetResponseStream()))
                {
                    text = reader.ReadToEnd();
                }
                return text;
            }
            catch (Exception ex)
            {
                return Url;
            }
        }

        public static string MimeType(string Extension)
        {
            string mime = "application/octetstream";
            if (string.IsNullOrEmpty(Extension))
                return mime;

            string ext = Extension.ToLower();
            Microsoft.Win32.RegistryKey rk = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (rk != null && rk.GetValue("Content Type") != null)
                mime = rk.GetValue("Content Type").ToString();
            return mime;
        }

        public static string SafeHTML(string pStrHTML)
        {
            System.Text.RegularExpressions.Regex lObjRegExp = null;
            if (pStrHTML == null)
            {
                return "";
            }
            if (pStrHTML == "")
            {
                return "";
            }

            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "<( )*script([^>])*>", "<script>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<( )*(/)( )*script( )*>)", "</script>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<script>).*(</script>)", "|||", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "<( )*SCRIPT([^>])*>", "<script>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<( )*(/)( )*SCRIPT( )*>)", "</script>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<SCRIPT>).*(</SCRIPT>)", "|||", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "<( )*meta([^>])*>", "<meta>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<( )*(/)( )*meta( )*>)", "</meta>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<meta>).*(</meta>)", "|||", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "<( )*META([^>])*>", "<META>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<( )*(/)( )*META( )*>)", "</META>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<META>).*(</META>)", "|||", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "<( )*iframe([^>])*>", "<iframe>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<( )*(/)( )*iframe( )*>)", "</iframe>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<iframe>).*(</iframe>)", "|||", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "<( )*IFRAME([^>])*>", "<IFRAME>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<( )*(/)( )*IFRAME( )*>)", "</IFRAME>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<IFRAME>).*(</IFRAME>)", "|||", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "<( )*frameset([^>])*>", "<frameset>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<( )*(/)( )*frameset( )*>)", "</frameset>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<frameset>).*(</frameset>)", "|||", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "<( )*FRAMESET([^>])*>", "<FRAMESET>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<( )*(/)( )*FRAMESET( )*>)", "</FRAMESET>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<FRAMESET>).*(</FRAMESET>)", "|||", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "<( )*frame([^>])*>", "<frame>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<( )*(/)( )*frame( )*>)", "</frame>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<frame>).*(</frame>)", "|||", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "<( )*FRAME([^>])*>", "<FRAME>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<( )*(/)( )*FRAME( )*>)", "</FRAME>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<FRAME>).*(</FRAME>)", "|||", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "<( )*link([^>])*>", "<link>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<( )*(/)( )*link( )*>)", "</link>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<link>).*(</link>)", "|||", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "<( )*LINK([^>])*>", "<LINK>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<( )*(/)( )*LINK( )*>)", "</LINK>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<LINK>).*(</LINK>)", "|||", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "<( )*style([^>])*>", "<style>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<( )*(/)( )*style( )*>)", "</style>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<style>).*(</style>)", "|||", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "<( )*STYLE([^>])*>", "<STYLE>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<( )*(/)( )*STYLE( )*>)", "</STYLE>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<STYLE>).*(</STYLE>)", "|||", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "<( )*object([^>])*>", "<object>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<( )*(/)( )*object( )*>)", "</object>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<object>).*(</object>)", "|||", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "<( )*OBJECT([^>])*>", "<OBJECT>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<( )*(/)( )*OBJECT( )*>)", "</OBJECT>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<OBJECT>).*(</OBJECT>)", "|||", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "<( )*applet([^>])*>", "<applet>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<( )*(/)( )*applet( )*>)", "</applet>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<applet>).*(</applet>)", "|||", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "<( )*APPLET([^>])*>", "<APPLET>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<( )*(/)( )*APPLET( )*>)", "</APPLET>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<APPLET>).*(</APPLET>)", "|||", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "<( )*input([^>])*>", "<input>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<( )*(/)( )*input( )*>)", "</input>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<input>).*(</input>)", "|||", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "<( )*INPUT([^>])*>", "<INPUT>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<( )*(/)( )*INPUT( )*>)", "</INPUT>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            pStrHTML = System.Text.RegularExpressions.Regex.Replace(pStrHTML, "(<INPUT>).*(</INPUT>)", "|||", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            lObjRegExp = new System.Text.RegularExpressions.Regex("<(/)?SCRIPT|META|STYLE([^>]*)>");
            pStrHTML = lObjRegExp.Replace(pStrHTML, "|||");
            lObjRegExp = new System.Text.RegularExpressions.Regex("<(/)?script|meta|style([^>]*)>");
            pStrHTML = lObjRegExp.Replace(pStrHTML, "|||");
            lObjRegExp = new System.Text.RegularExpressions.Regex("<(/)?(LINK|IFRAME|FRAMESET|FRAME|APPLET|OBJECT)([^>]*)>");
            pStrHTML = lObjRegExp.Replace(pStrHTML, "|||");
            lObjRegExp = new System.Text.RegularExpressions.Regex("<(/)?(link|iframe|frameset|frame|applet|object)([^>]*)>");
            pStrHTML = lObjRegExp.Replace(pStrHTML, "|||");
            lObjRegExp = new System.Text.RegularExpressions.Regex("(<A[^>]+HREF\\s?=\\s?\"?javascript:)[^\"]*(\"[^>]+>)");
            pStrHTML = lObjRegExp.Replace(pStrHTML, "|||");
            lObjRegExp = new System.Text.RegularExpressions.Regex("(<a[^>]+href\\s?=\\s?\"?javascript:)[^\"]*(\"[^>]+>)");
            pStrHTML = lObjRegExp.Replace(pStrHTML, "|||");
            lObjRegExp = new System.Text.RegularExpressions.Regex("(<IMG[^>]+SRC\\s?=\\s?\"?javascript:)[^\"]*(\"[^>]+>)");
            pStrHTML = lObjRegExp.Replace(pStrHTML, "|||");
            lObjRegExp = new System.Text.RegularExpressions.Regex("(<img[^>]+src\\s?=\\s?\"?javascript:)[^\"]*(\"[^>]+>)");
            pStrHTML = lObjRegExp.Replace(pStrHTML, "|||");
            lObjRegExp = new System.Text.RegularExpressions.Regex("<([^>]*) on[^=\\s]+\\s?=\\s?([^>]*)>");
            pStrHTML = lObjRegExp.Replace(pStrHTML, "|||");
            lObjRegExp = new System.Text.RegularExpressions.Regex("<(/)?(SCRIPT|META|STYLE|INPUT|LINK|IFRAME|FRAMESET|FRAME|APPLET|OBJECT)([^>]*)>");
            pStrHTML = lObjRegExp.Replace(pStrHTML, "|||");
            lObjRegExp = new System.Text.RegularExpressions.Regex("<(/)?(script|meta|style|input|link|iframe|frameset|frame|applet|object)([^>]*)>");
            pStrHTML = lObjRegExp.Replace(pStrHTML, "|||");
            lObjRegExp = new System.Text.RegularExpressions.Regex("(Java|J|VB)(Script)");
            pStrHTML = lObjRegExp.Replace(pStrHTML, "|||");
            lObjRegExp = new System.Text.RegularExpressions.Regex("(java|j|vb)(Script)");
            pStrHTML = lObjRegExp.Replace(pStrHTML, "|||");
            lObjRegExp = null;
            string tmpString = "";
            pStrHTML = pStrHTML.Trim();
            //pStrHTML = pStrHTML.Replace(string.Chr(59), " ");
            pStrHTML = pStrHTML.Replace("--", " ");
            pStrHTML = pStrHTML.Replace("DECLARE", " ");
            pStrHTML = pStrHTML.Replace("declare", " ");
            pStrHTML = pStrHTML.Replace("CHAR(", " ");
            pStrHTML = pStrHTML.Replace("char(", " ");
            pStrHTML = pStrHTML.Replace("EXEC(", " ");
            pStrHTML = pStrHTML.Replace("exec(", " ");
            return pStrHTML;
        }
    }
}